---
title: Woensdag 11 oktober 2017
date: 2017-10-11
---
Vandaag hadden we een studiodag. We hebben alle taken voor in de vakantie verdeeld en we hebben ons zelfs al bezig gehouden met de Expo. Een aantal teamleden hielden zich bezig met de poster en het echte prototype. We hebben met elkaar gediscussieerd hoe we alles willen hebben. Ik heb me vervolgens ook beziggehouden met het concept. Ik heb de laatste details uitgewerkt en ervoor gezorgd dat het voor ons duidelijk is.
