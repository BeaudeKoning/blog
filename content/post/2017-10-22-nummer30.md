---
title: Vrijdag 13 oktober t/m zondag 22 oktober = VAKANTIE
date: 2017-10-22
---
Van vrijdag 13 oktober tot en met zondag 22 oktober hadden we vakantie. In de vakantie heb ik me vooral beziggehouden met mijn leerdossier en het blog. Ik ben dus verschillende STARRT's gaan schrijven en ik heb ervoor gezorgd dat ik op schema liep met mijn blog. Daarnaast heb ik me in de vakantie beziggehouden met Tools for design Photoshop en Indesign.  Tot slot hebben we met het team de laatste dingetjes voor de expositie besproken. In de vakantie heb ik vooral veel geleerd van het schrijven van het leerdossier. Ik haalde nu weer even alles op wat ik afgelopen tijd heb geleerd en wat ik mee ga nemen naar het volgende kwartaal.
