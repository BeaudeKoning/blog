---
title: Dinsdag 21 november 2017
date: 2017-11-21
---
Vandaag hadden we in de ochtend een hoorcollege. Deze ging over de mens, en dan gericht op de persoonlijke beïnvloeding. Ik heb hier enorm veel geleerd en ik merk dat dit een onderwerp is wat mij aanspreekt. We hebben geleerd wat er van invloed is op gedrag en dat we als ontwerper veel van de doelgroep af moeten weten. Ook zijn verschillende theorieën aan bod gekomen. Daarnaast hebben we het gehad over intrinsieke en extrensieke motivatie en hebben we gesproken over persoonlijkheden. Na het hoorcollege ben ik naar huis gegaan en heb ik gewerkt aan mijn moodboards. In de middag heb ik het keuzevak 'ontwerp je eigen merkidentiteit' gevolgd.
 