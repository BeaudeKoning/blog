---
title: Maandag 4 september 2017
date: 2017-09-04
tags: ["Periode1"]
---

Maandag 4 september was de eerste schooldag. Iedereen van ons groepje had thuis van tevoren al onderzoeksvragen bedacht. Op deze manier konden we maandagochtend gelijk een start maken aan iteratie 1. We zijn als eerste onderzoek gaan doen naar verschillende studies. Op deze manier konden we een doelgroep uitkiezen. Onze keuze was na veel onderzoek gevallen op fysiotherapie omdat we echt iets heel anders wilden dan onze eigen opleiding CMD. Daarnaast vonden we het interessant om het menselijk lichaam en sport te verwerken in het spel. We wilden ons echt gaan verdiepen in een opleiding waar we niet veel vanaf wisten, op deze manier was het echt een uitdaging. 
Vervolgens hebben we de onderzoeksvragen met elkaar gedeeld en de beste uitgekozen. Ik vond dit erg fijn, want op deze manier leerde ik echt hoe mijn teamleden over de opdracht dachten. 
Vervolgens hebben we de onderzoeksvragen onderverdeeld. Ik ben me gaan focussen op de doelgroep. Ik ben op verschillende websites eerst onderzoek gaan doen naar fysiotherapie in het algemeen. Ik heb genoteerd wat de opleiding inhield en belangrijke kenmerken van een fysiotherapiestudent. Na het onderzoek had ik nog genoeg vragen waarin ik me wilde verdiepen. Ik heb toen 11 interviewvragen bedacht voor de doelgroep. 
Voorderest hebben we ons deze dag beziggehouden met het maken van een taakverdeling en hebben we aan een poster gewerkt. Ik vond dit interessant omdat je op deze manier je teamleden nog beter leerde kennen.This is my first post, how exciting!!!!!