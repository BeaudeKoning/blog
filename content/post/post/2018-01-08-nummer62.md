---
title: Maandag 8 januari 2018
date: 2018-01-08
---
Vandaag was weer de eerste studiodag na de vakantie. Vandaag was de workshop Design Rationale en ik ben hier naartoe geweest. Het was een korte workshop. We kregen te horen wat het was en we kregen een voorbeeld mee. Daarna mocht je zelf verder aan de slag gaan. We liepen met het team ook redelijk goed op schema, dus we zijn vandaag gewoon rustig verder gaan werken.