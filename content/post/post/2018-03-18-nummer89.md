---
title: Zondag 18-03-18
date: 2018-03-18
---
Vandaag heb ik de hele dag aan school gewerkt. Ik ben me in gaan schrijven voor verschillende workshops, heb het hele opzetje voor mijn leerdossier gemaakt, heb 2 STARRT's geschreven, voor mijn tentamen geleerd en aan het project gewerkt. Voor het project heb ik twee creatieve technieken toegepast > de superhelden techniek en de bloemassociatie. Ik vond het interessant op met verschillende creatieve technieken te werken. Ook heb ik trello eventjes bijgewerkt.