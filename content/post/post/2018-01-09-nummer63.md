---
title: Dinsdag 9 januari 2018
date: 2018-01-09
---
Vandaag ben ik in de ochtend naar het hoorcollege geweest. Het was een leerzaam hoorcollege. Daarna ben ik op school gebleven en heb ik wat geleerd voor het tentamen. Vervolgens had ik van 1 tot 3 de workshop testplan high fid prototype. Dit was erg leerzaam. We namen stap voor de stap de belangrijke punten door en we kregen echt goed te horen wat van belang is bij het testen van een prototype en welke vragen je wel en niet mag stellen. Na de workshop ben ik ook op school gebleven en daarna ben ik van 5 tot half 7 naar mijn keuzevak geweest. In de avond ben ik verder gegaan met studeren.