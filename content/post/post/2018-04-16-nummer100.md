---
title: Maandag 16 april 2018
date: 2018-04-16
---
Vandaag hebben we in de ochtend een teamgesprek gehad bij Mieke. Ik was samen met Aidan en Ihsan. Ik vond het een erg fijn gesprek. We konden alle positieve en negatieve dingen in het team even met elkaar bespreken en uitleggen hoe we ons in het team voelde. Op deze manier konden we echt weer met een frisse nieuwe start beginnen en werken aan de dingen die minder goed gingen. Het was echt prettig. In de middag ben ik aan de slag gegaan met de aanpassingen van het PvA en heb ik de debriefing helemaal afgemaakt. Ook heb ik me voorbereid op de testen die we op vrijdag 20 april willen gaan uitvoeren. 